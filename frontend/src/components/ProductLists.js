import axios from 'axios';
import React, { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Store } from '../Store';
import Rating from './Rating';

export default function ProductLists(props) {
  const { product } = props;

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    userInfo,
    cart: { cartItems },
  } = state;

  const addToCartHandler = async item => {
    const existItem = cartItems.find(x => x._id === product._id);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const { data } = await axios.get(`/products/${item._id}`);
    if (data.countInStock < quantity) {
      toast.error('Sorry, Product is out of Stock!');
      return;
    }
    // const response = await fetch(
    //   `http://localhost:4000/api/products/${product._id}`
    // );
    // const data = await response.json();

    // if (data.countInStock < quantity) {
    // toast.error('Sorry, Product is out of Stock!');
    //   return;
    // }

    ctxDispatch({
      type: 'CART_ADD_ITEM',
      payload: { ...item, quantity },
    });
    toast.success(`${data.name} is added to your cart.`);
  };

  return (
    <Card key={product.slug} className="card-product ">
      <Link to={`/products/${product.slug}`}>
        <img
          src={product.image}
          className="card-img-top p-3"
          alt={product.name}
        />
      </Link>
      <Card.Body>
        <Link to={`/products/${product.slug}`}>
          <Card.Title>{product.name}</Card.Title>
        </Link>
        <Rating rating={product.rating} numReviews={product.numReviews} />
        <Card.Text>₱ {product.price}</Card.Text>

        {userInfo && userInfo.isAdmin ? (
          <Button
            className="add-cart-btn"
            onClick={() => addToCartHandler(product)}
            disabled
          >
            Add to cart
          </Button>
        ) : product.countInStock === 0 ? (
          <Button variant="light" disabled>
            Out of stock
          </Button>
        ) : (
          <Button
            className="add-cart-btn"
            onClick={() => addToCartHandler(product)}
          >
            Add to cart
          </Button>
        )}
      </Card.Body>
    </Card>
  );
}
