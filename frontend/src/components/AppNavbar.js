import React, { useContext } from 'react';
import { Badge, Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import { Store } from '../Store';
import SearchBox from './SearchBox';

export default function AppNavbar() {
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart, userInfo } = state;

  const logoutHandler = () => {
    ctxDispatch({ type: 'USER_LOGOUT' });
    localStorage.removeItem('userInfo');
    localStorage.removeItem('shippingAddress');
    localStorage.removeItem('paymentMethod');
    window.location.href = '/login';
  };

  return (
    <div>
      <header>
        <Navbar className="shadow-sm fixed-top nav-container" expand="lg">
          <Container>
            <LinkContainer to="/">
              <Navbar.Brand className="brand-logo">
                <img src="../images/carmaj_logo.png" alt="Logo" />
              </Navbar.Brand>
            </LinkContainer>
            <Navbar.Toggle
              aria-controls="basic-navbar-nav"
              className="basic-navbar-nav"
            />
            <Navbar.Collapse id="basic-navbar-nav">
              <SearchBox />
              <Nav className="me-auto w-100 justify-content-end">
                <Link to="/" className="nav-link me-3">
                  Home
                </Link>

                {userInfo && userInfo.isAdmin && (
                  <Link to="/products" className="nav-link me-3">
                    Product
                  </Link>
                )}

                <div>
                  {userInfo ? (
                    <NavDropdown
                      title={userInfo.firstName + ' ' + userInfo.lastName}
                    >
                      <LinkContainer to="/profile">
                        <NavDropdown.Item>My Profile</NavDropdown.Item>
                      </LinkContainer>
                      <LinkContainer to="/orderhistory">
                        <NavDropdown.Item>Order History</NavDropdown.Item>
                      </LinkContainer>
                      <NavDropdown.Divider />
                      <Link
                        className="dropdown-item"
                        to="#logout"
                        onClick={logoutHandler}
                      >
                        Logout
                      </Link>
                    </NavDropdown>
                  ) : (
                    <Link className="nav-link me-3" to="/login">
                      Login
                    </Link>
                  )}
                </div>

                {userInfo && userInfo.isAdmin && (
                  <NavDropdown title="Admin" id="admin-nav-dropdown">
                    <NavDropdown.Header>Dashboard</NavDropdown.Header>
                    <LinkContainer to="/admin/products">
                      <NavDropdown.Item>Products</NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to="/admin/orders">
                      <NavDropdown.Item>Orders</NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to="/admin/users">
                      <NavDropdown.Item>Users</NavDropdown.Item>
                    </LinkContainer>
                  </NavDropdown>
                )}

                <Link to="/cart" className="nav-link">
                  <i className="fa-sharp fa-solid fa-cart-shopping"></i>
                  {cart.cartItems.length > 0 && (
                    <Badge pill className="primary-color">
                      {cart.cartItems.reduce(
                        (tempValue, currentValue) =>
                          tempValue + currentValue.quantity,
                        0
                      )}
                    </Badge>
                  )}
                </Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </header>
    </div>
  );
}
