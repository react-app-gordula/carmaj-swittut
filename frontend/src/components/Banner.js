import React from 'react';

export default function Banner() {
  return (
    <div className=" text-center div-container">
      <img
        src="./images/carmaj_banner.png"
        alt="banner"
        className="carmaj-banner"
      ></img>
    </div>
  );
}
