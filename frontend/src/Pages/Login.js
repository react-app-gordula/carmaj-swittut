import Axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getError } from '../error';
import { Store } from '../Store';

export default function Login() {
  const navigate = useNavigate();
  const { search } = useLocation();
  // the value in 'redirectInUrl' is = /shipping if exist, if not it will go to the default redirect which is the home page
  const redirectInUrl = new URLSearchParams(search).get('redirect');
  const redirect = redirectInUrl ? redirectInUrl : '/';

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState('');

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { userInfo } = state;

  // using Axios
  const submitHandler = async e => {
    e.preventDefault();
    try {
      const { data } = await Axios.post('/users/login', {
        email,
        password,
      });
      ctxDispatch({ type: 'USER_LOGIN', payload: data });
      toast.success('Your are now logged in.');
      localStorage.setItem('userInfo', JSON.stringify(data));
      navigate(redirect || '/');
      // console.log(data);
    } catch (err) {
      toast.error(getError(err));
    }
  };

  // using fetch
  // const submitHandler = async e => {
  //   e.preventDefault();
  //   try {
  //     const response = await fetch('/api/users/login', {
  //       method: 'POST',
  //       body: { email, password },
  //     });
  //     const jsonResponse = await response.json();
  //     ctxDispatch({ type: 'USER_LOGIN', payload: jsonResponse.data });
  //     localStorage.setItem('userInfo', JSON.stringify(jsonResponse.data));
  //     navigate(redirect || '/');
  //   } catch (err) {
  //     toast.error(getError(err));
  //   }
  // };

  // if user already login it will not to go the /login page
  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <div className="div-container">
      <Container className="small-container shadow p-3 mb-5 bg-white rounded">
        <Helmet>
          <title>Carmaj Swittut | Login</title>
        </Helmet>
        <h2 className="my-3">Login</h2>
        <Form onSubmit={submitHandler}>
          <Form.Group className="mb-3" controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Email Address"
              value={email}
              onChange={e => setEmail(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <div className="mb-3">
            {isActive ? (
              <Button type="submit" className="login-btn">
                Login
              </Button>
            ) : (
              <Button type="submit" className="login-btn" disabled>
                Login
              </Button>
            )}
          </div>
          <div className="mb-3">
            New Customer?{' '}
            <Link
              to={`/register?redirect=${redirect}`}
              className="text-decoration-none"
            >
              Create your account
            </Link>
          </div>
        </Form>
      </Container>
    </div>
  );
}
