import Axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getError } from '../error';
import { Store } from '../Store';

export default function Register() {
  const navigate = useNavigate();
  const { search } = useLocation();

  // the value in 'redirectInUrl' is = /shipping if exist and if not it will go to the default redirect which is the home page
  const redirectInUrl = new URLSearchParams(search).get('redirect');

  const redirect = redirectInUrl ? redirectInUrl : '/';

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { userInfo } = state;

  const submitHandler = async e => {
    e.preventDefault();
    if (password !== confirmPassword) {
      toast.error('Password do not match');
      return;
    }
    try {
      const { data } = await Axios.post('/users/register', {
        firstName,
        lastName,
        mobileNo,
        email,
        password,
      });
      //console.log(data);
      ctxDispatch({ type: 'USER_LOGIN', payload: data });
      toast.success('Thank you for registering!');
      localStorage.setItem('userInfo', JSON.stringify(data));
      navigate(redirect || '/');
    } catch (err) {
      toast.error(getError(err));
    }
  };

  // if user already login it will not to go the /login page
  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);

  return (
    <div className="mb-5 div-container">
      <Container className="shadow p-3 mb-5 bg-white rounded">
        <Helmet>
          <title>Carmaj Swittut | Register</title>
        </Helmet>
        <h2 className="my-3">Register</h2>
        <Form onSubmit={submitHandler}>
          <Row>
            <Col>
              <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="First Name"
                  onChange={e => setFirstName(e.target.value)}
                  required
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Last Name"
                  onChange={e => setLastName(e.target.value)}
                  required
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="+xxx-xxx-xxxx"
                  onChange={e => setMobileNo(e.target.value)}
                  required
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Email Address"
                  onChange={e => setEmail(e.target.value)}
                  required
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={e => setPassword(e.target.value)}
                  required
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group className="mb-3" controlId="confirmPassword">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  onChange={e => setConfirmPassword(e.target.value)}
                  required
                />
              </Form.Group>
            </Col>
          </Row>

          <div className="mb-3">
            <Button type="submit" className="login-btn">
              Register
            </Button>
          </div>
          <div className="mb-3">
            Already registered?{' '}
            <Link
              to={`/login?redirect=${redirect}`}
              className="text-decoration-none"
            >
              Login
            </Link>
          </div>
        </Form>
      </Container>
    </div>
  );
}
