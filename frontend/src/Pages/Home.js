import React, { useEffect, useReducer, useState } from 'react';
import Banner from '../components/Banner';
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';
import ProductList from '../components/ProductList';
import { Helmet } from 'react-helmet-async';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';

const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return { ...state, products: action.payload, loading: false };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default function Home() {
  const [{ loading, error, products }, dispatch] = useReducer(reducer, {
    products: [],
    loading: true,
    error: '',
  });

  // useEffect(() => {
  //   const fetchData = async () => {
  //     dispatch({ type: 'FETCH_REQUEST' });
  //     try {
  //       const response = await fetch(`http://localhost:4000/api/products`);
  //       const data = await response.json();
  //       dispatch({ type: 'FETCH_SUCCESS', payload: data });
  //       console.log(data);
  //     } catch (err) {
  //       dispatch({ type: 'FETCH_FAIL', payload: err.message });
  //     }
  //   };
  //   fetchData();
  // }, []);

  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'FETCH_REQUEST' });
      try {
        const result = await axios.get('/products');
        dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
        console.log(result);
      } catch (err) {
        dispatch({ type: 'FETCH_FAIL', payload: err.message });
      }
    };
    fetchData();
  }, []);

  return (
    <>
      <div className="my-5 div-container">
        <Helmet>
          <title>Carmaj Swittut | Home</title>
        </Helmet>
        <Banner />

        <div className="products">
          <h1 className="mt-5 mb-4 text-center our-product">
            Featured Products
          </h1>
          {loading ? (
            <LoadingBox />
          ) : error ? (
            <MessageBox variant="danger">{error}</MessageBox>
          ) : (
            <Row>
              {products.map(product => (
                <Col key={product.slug} sm={6} md={4} lg={3} className="mb-3">
                  <ProductList product={product}></ProductList>
                </Col>
              ))}
            </Row>
          )}
        </div>
      </div>
    </>
  );
}
