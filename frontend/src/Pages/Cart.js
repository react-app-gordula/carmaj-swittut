import { useContext } from 'react';
import { Store } from '../Store';
import { Helmet } from 'react-helmet-async';
import {
  Row,
  Col,
  ListGroup,
  Button,
  Card,
  ListGroupItem,
} from 'react-bootstrap';
import MessageBox from '../components/MessageBox';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { toast } from 'react-toastify';

export default function Cart() {
  const navigate = useNavigate();
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    cart: { cartItems },
  } = state;

  const updateCartHandler = async (item, quantity) => {
    const { data } = await axios.get(`/products/${item._id}`);
    if (data.countInStock < quantity) {
      toast.error('Sorry, Product is out of Stock!');
      return;
    }
    // const response = await fetch(
    //   `http://localhost:4000/api/products/${product._id}`
    // );
    // const data = await response.json();

    // if (data.countInStock < quantity) {
    // toast.error('Sorry, Product is out of Stock!');
    //   return;
    // }

    ctxDispatch({
      type: 'CART_ADD_ITEM',
      payload: { ...item, quantity },
    });
  };

  const removeItemHandler = item => {
    ctxDispatch({ type: 'CART_REMOVE_ITEM', payload: item });
  };

  const checkoutHandler = () => {
    navigate('/login?redirect=/shipping');
  };

  return (
    <div className="div-container">
      <Helmet>
        <title>Carmaj Swittut | Shopping Cart</title>
      </Helmet>
      <h2>My Shopping Cart</h2>
      <Row>
        <Col md={8} className="my-3">
          {cartItems.length === 0 ? (
            <MessageBox>
              No Items Available at your bag.{' '}
              <Link to="/" className="text-decoration-none">
                Go Shopping
              </Link>
            </MessageBox>
          ) : (
            <ListGroup>
              {cartItems.map(item => (
                <ListGroup.Item key={item._id}>
                  <Row className="align-items-center">
                    <Col md={5}>
                      <img
                        src={item.image}
                        alt={item.image}
                        className="img-fluid rounded img-thumbnail"
                      ></img>{' '}
                      <Link to={`/products/${item.slug}`}>{item.name}</Link>
                    </Col>
                    <Col md={3}>
                      {/* decreasing item in the cart */}
                      <Button
                        variant="light"
                        onClick={() =>
                          updateCartHandler(item, item.quantity - 1)
                        }
                        disabled={item.quantity === 1}
                      >
                        <i className="fas fa-minus-circle"></i>
                      </Button>
                      <span>{item.quantity}</span>{' '}
                      {/* increasing item in the cart */}
                      <Button
                        variant="light"
                        onClick={() =>
                          updateCartHandler(item, item.quantity + 1)
                        }
                        disabled={item.quantity === item.countInStock}
                      >
                        <i className="fas fa-plus-circle"></i>
                      </Button>
                    </Col>
                    <Col md={2}>₱{item.price}</Col>
                    <Col md={2}>
                      <Button
                        variant="light"
                        onClick={() => removeItemHandler(item)}
                      >
                        <i className="fas fa-trash"></i>
                      </Button>
                    </Col>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
          )}
        </Col>
        <Col md={4} className="my-3">
          <Card>
            <Card.Body>
              <ListGroup variant="flush">
                <ListGroupItem>
                  <h4 className="text-center">
                    Subtotal (
                    {cartItems.reduce(
                      (tempValue, currentValue) =>
                        tempValue + currentValue.quantity,
                      0
                    )}{' '}
                    items) : ₱
                    {cartItems.reduce(
                      (tempValue, currentValue) =>
                        tempValue + currentValue.price * currentValue.quantity,
                      0
                    )}
                  </h4>
                </ListGroupItem>
                <ListGroupItem>
                  <div className="d-grid">
                    <Button
                      className="checkout-btn"
                      type="button"
                      onClick={checkoutHandler}
                      disabled={cartItems.length === 0}
                    >
                      Proceed to Checkout
                    </Button>
                  </div>
                </ListGroupItem>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
