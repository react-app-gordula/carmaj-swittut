// import bcrypt from 'bcryptjs';

const data = {
  // users: [
  //   {
  //     firstName: 'John',
  //     lastName: 'Doe',
  //     mobileNo: '09201112222',
  //     email: 'user@mail.com',
  //     password: bcrypt.hashSync('user123'),
  //     isAdmin: false,
  //   },
  //   {
  //     firstName: 'Dora',
  //     lastName: 'Explorer',
  //     mobileNo: '09991112222',
  //     email: 'admin@mail.com',
  //     password: bcrypt.hashSync('admin123'),
  //     isAdmin: true,
  //   },
  // ],
  products: [
    {
      // _id: '1',
      name: 'Choco Overload Cake',
      slug: 'choco-overload-cake',
      category: 'Cake',
      image: '/images/chocoload.png',
      price: 1200,
      countInStock: 10,
      rating: 4.5,
      numReviews: 10,
      description:
        'Layers of different textures of chocolate cake with various chocolate fillings and toppings.',
    },
    {
      // _id: '2',
      name: 'Mango Bravo Cake',
      slug: 'mango-bravo-cake',
      category: 'Cake',
      image: '/images/mangobravo.png',
      price: 1500,
      countInStock: 15,
      rating: 5,
      numReviews: 20,
      description:
        'Layers of crunchy wafers filled with chocolate mousse, cream and mango cubes.',
    },
    {
      // _id: '3',
      name: 'Ube Cake',
      slug: 'ube-cake',
      category: 'Cake',
      image: '/images/ubecake.png',
      price: 1300,
      countInStock: 0,
      rating: 4.2,
      numReviews: 14,
      description:
        'Ube sponge cake with real purple yam and layers of creamy icing on top.',
    },
    {
      // _id: '4',
      name: 'Cheesy Ensaymada',
      slug: 'cheesy-ensaymada',
      category: 'dessert',
      image: '/images/cheesymada.png',
      price: 80,
      countInStock: 5,
      rating: 4.1,
      numReviews: 8,
      description:
        'Soft, sweet dough pastry covered with butter and sugar then topped with lots of grated cheese.',
    },
    {
      // _id: '5',
      name: 'Red Velvet Cupcake',
      slug: 'red-velvet-cupcake',
      category: 'cupcake',
      image: '/images/redvelvet.png',
      price: 130,
      countInStock: 10,
      rating: 5,
      numReviews: 15,
      description:
        'A very dramatic looking cake with a bright red colour contrasted with white icing.',
    },
  ],
};

export default data;
