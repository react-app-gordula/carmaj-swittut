import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Container } from 'react-bootstrap';
import Home from './Pages/Home';
import Product from './Pages/Product';
import Cart from './Pages/Cart';
import Login from './Pages/Login';
import ShippingAddress from './Pages/ShippingAddress';
import Register from './Pages/Register';
import PaymentMethod from './Pages/PaymentMethod';
import PlaceOrder from './Pages/PlaceOrder';
import Order from './Pages/Order';
import NotFound from './Pages/NofFound';
import PrivateRoute from './components/PrivateRoute';
import Profile from './Pages/Profile';
import AdminRoute from './components/AdminRoute';
import ProductList from './Pages/ProductList';
import ProductEdit from './Pages/ProductEdit';
import OrderHistory from './Pages/OrderHistory';
import OrderList from './Pages/OrderList';
import UserList from './Pages/UserList';
import UserEdit from './Pages/UserEdit';
import AppNavbar from './components/AppNavbar';
import Products from './Pages/Products';
// import SearchPage from './Pages/SearchPage';
// import Footer from './components/Footer';

function App() {
  return (
    <Router>
      <div className="d-flex flex-column app-container">
        <ToastContainer
          position="top-right"
          autoClose={2000}
          hideProgressBar={true}
          limit={1}
        ></ToastContainer>
        <AppNavbar />
        <main>
          <Container className="my-5">
            <Routes>
              <Route>
                <Route path="/" element={<Home />} />
                <Route path="/products/:slug" element={<Product />} />
                <Route path="/products/" element={<Products />} />
                <Route path="/cart" element={<Cart />} />
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/shipping" element={<ShippingAddress />} />
                <Route path="/payment" element={<PaymentMethod />} />
                <Route path="/placeorder" element={<PlaceOrder />} />
                <Route path="/order/:id" element={<Order />} />
                <Route path="/orderhistory" element={<OrderHistory />} />
                {/* <Route path="/search" element={<SearchPage />} /> */}

                {/* Admin Routes */}
                <Route
                  path="/profile"
                  element={
                    <PrivateRoute>
                      <Profile />
                    </PrivateRoute>
                  }
                />

                <Route
                  path="/admin/orders"
                  element={
                    <AdminRoute>
                      <OrderList />
                    </AdminRoute>
                  }
                ></Route>

                <Route
                  path="/admin/users"
                  element={
                    <AdminRoute>
                      <UserList />
                    </AdminRoute>
                  }
                ></Route>

                <Route
                  path="/admin/user/:id"
                  element={
                    <AdminRoute>
                      <UserEdit />
                    </AdminRoute>
                  }
                ></Route>

                <Route
                  path="/admin/products"
                  element={
                    <AdminRoute>
                      <ProductList />
                    </AdminRoute>
                  }
                ></Route>

                <Route
                  path="/admin/product/:id"
                  element={
                    <AdminRoute>
                      <ProductEdit />
                    </AdminRoute>
                  }
                ></Route>

                <Route path="*" element={<NotFound />} />
              </Route>
            </Routes>
          </Container>
        </main>

        {/* <footer className="mt-5 mb-3">
          <div className="text-center">
            &#169; 2022 Carmaj Swittut. All rights reserved
          </div>
        </footer> */}
      </div>
    </Router>
  );
}

export default App;
